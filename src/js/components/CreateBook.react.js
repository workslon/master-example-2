var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var BookModel = require('../models/Book');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.isbn).focus();
  },

  componentDidUpdate: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    if (status === 'success') {
      var refs = this.refs;
      refs.isbn.value = refs.title.value = refs.year.value = '';
    }
  },

  create: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};
    var edition = refs.edition || {};

    AppActions.createBook(BookModel, {
      isbn: isbn.value,
      title: title.value,
      year: parseInt(year.value),
      edition: edition.value
    });
  },

  validate: function(e) {
    AppActions.validate(BookModel, e.target.id, e.target.value);
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
      <form>
        <h3>Create Book</h3>
        <div className="form-group">
          <label htmlFor="isbn">ISBN</label>
          <input defaultValue="" onInput={this.validate} ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
          {errors.isbn && <span className="text-danger">{errors.isbn}</span>}
        </div>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input defaultValue="" onInput={this.validate} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
          {errors.title && <span className="text-danger">{errors.title}</span>}
        </div>
        <div className="form-group">
          <label htmlFor="year">Year</label>
          <input defaultValue="" onInput={this.validate} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
          {errors.year && <span className="text-danger">{errors.year}</span>}
        </div>
        <div className="form-group">
          <label htmlFor="edition">Edition</label>
          <input defaultValue="" onInput={this.validate} ref="edition" type="text" className="form-control" id="edition" placeholder="Edition" />
          {errors.edition && <span className="text-danger">{errors.edition}</span>}
        </div>
        <button type="submit" onClick={this.create} className="btn btn-default">Submit</button>
        {status === 'success' && <p className="bg-success">Success!</p>}
        {status === 'pending' && <p className="bg-info">Creating...</p>}
        <IndexLink className="back" to="/">&laquo; back</IndexLink>
      </form>
    );
  }
});