var AppDispatcher = require('../dispatchers/AppDispatcher');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';

var log = {
  type: '',
  message: ''
};

var AppStore = Object.assign({}, EventEmitter.prototype, {
  getMessage: function () {
    return log.message;
  },

  getType: function () {
    return log.type;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  var type = action.type.toLowerCase();

  log.message = JSON.stringify(action).replace(/\\"/g, '"').replace(/\n/g, '');

  if (~type.indexOf('success') || ~type.indexOf('clear')) {
    log.type = 'success';
  } else if (~type.indexOf('error') || ~type.indexOf('non_unique')) {
    log.type = 'danger';
  } else {
    log.type = 'info';
  }

  AppStore.emitChange();
});

module.exports = AppStore;